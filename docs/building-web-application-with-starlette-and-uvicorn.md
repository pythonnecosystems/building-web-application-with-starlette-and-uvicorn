# Starlette 및 Uvicorn으로 웹 애플리케이션 구축하기: 초보자를 위한 튜토리얼 <sup>[1](#footnote_1)</sup>

## Starlette
Starlette은 고성능 Python 어플리케이션을 구축하기 위한 빠르고 가벼운 비동기식 웹 프레임워크이다. 간단하고 효율적으로 설계되어 확장 가능하고 강력한 웹 서비스를 구축하는 데 필요한 도구를 제공한다. 
Starlette은 `asyncio`라는 비동기 프레임워크를 기반으로 구축되어 동시에 여러 연결을 효율적으로 처리할 수 있다.

### Flask와 비교하여 Starlette의 장점은?
Starlette과 Flask는 모두 Python 생태계에서 인기 있는 웹 프레임워크이지만, 디자인 철학과 기능이 다르다. 다음은 Flask에 비해 Starlette이 갖는 장점이다.

### 비동기 지원
Starlette는 처음부터 비동기 프로그래밍을 염두에 두고 개발되었다. 비동기 프레임워크를 활용하고 비동기 연산을 처리하기 위한 기능을 기본적으로 지원하므로 동시성이 높은 시나리오에 적합하다. 
반면 Flask는 주로 동기식이며 비동기 작업을 처리하기 위해 추가 라이브러리나 기술이 필요하다.

### 성능
비동기식 특성과 경량 설계로 인해 Starlette는 동시 많은 연결을 효율적으로 처리할 수 있다. 따라서 확장성과 응답성이 필요한 고성능 어플리케이션을 구축하는 데 적합하다. 
Flask는 여전히 성능이 뛰어나지만 동시성이 높은 시나리오의 경우에는 확장이 어려울 수 있다.

## WebSocket 지원
Starlette에는 WebSocket 연결 처리를 기본적으로 지원하므로 클라이언트와 서버 간의 양방향 통신이 가능하다. 
반면 Flask에서 WebSocket 기능을 사용하려면 추가 라이브러리 또는 확장 프로그램이 필요하다.

## 최신 Python 구문
Starlette은 최신 Python 구문을 사용하여 구축되었으며, 최신 버전의 Python에 도입된 새로운 언어 기능을 활용한다. 이를 통해 코드베이스가 더 깔끔하고 간결해지며 유지 관리가 더 쉬워진다. 반면 Flask는 오래된 프레임워크이기 때문에 약간 더 전통적인 구문을 사용할 수 있다.

## 미들웨어 지원
Starlette는 유연하고 강력한 미들웨어 시스템을 제공하여 요청과 응답을 전반적으로 수정할 수 있다. 이는 인증, 요청/응답 로깅, 크로스 커팅 문제 처리와 같은 작업에 유용할 수 있다. Flask도 미들웨어를 지원하지만 Starlette의 미들웨어 시스템이 더 고급스럽고 더 나은 유연성을 제공한다.

## 확장성과 생태계
Flask는 오래 전부터 사용되어 왔으며 확장 프로그램과 라이브러리로 구성된 대규모 에코시스템이 구축되어 있다. 그러나 Starlette는 점점 인기를 얻고 있으며 사용 가능한 확장 기능과 통합 기능의 수가 증가하고 있다. Flask 생태계가 더 성숙한 반면, Starlette는 현대적이고 성장하는 생태계를 제공한다.

## Uvicorn
Uvicorn은 Python으로 작성된 웹 어플리케이션을 실행하는 데 일반적으로 사용되는 ASGI(비동기 서버 게이트웨이 인터페이스) 서버이다. ASGI는 웹 서버와 Python 웹 어플리케이션 또는 프레임워크 간의 표준 인터페이스로, 비동기 통신과 HTTP 요청 처리를 가능하게 한다.

Uvicorn은 빠르고 가볍게 설계되었으며, 효율적인 비동기 처리를 위해 Python의 `asyncio` 라이브러리의 기능을 활용한다. 일반적으로 고성능과 비동기 기능으로 잘 알려진 FastAPI와 Starlette 같은 프레임워크와 함께 사용한다.

개발자는 Uvicorn을 사용하여 HTTP/1.1 및 HTTP/2 프로토콜 지원, WebSocket 처리, 개발 중 자동 리로딩 등의 이점을 활용하여 Python 웹 어플리케이션을 쉽게 배포할 수 있다. Uvicorn은 확장성과 동시성이 높은 워크로드 처리 능력으로 잘 알려져 있다.

Uvicorn을 사용하여 Python 웹 어플리케이션을 시작하려면 일반적으로 어플리케이션을 나타내는 모듈과 해당 모듈 내의 콜러블(callable)을 지정해야 한다. 예를 들어 어플리케이션이 `main.py`라는 파일에 정의되어 있고 콜러블의 이름이 `app`인 경우 다음 명령으로 어플리케이션을 시작할 수 있다.

```bash
$ uvicorn main:app
```

이 명령으로 Uvicorn 서버를 시작하고 기본 호스트(로컬 호스트)와 포트(8000)에 바인딩된다. 필요에 따라 사용자 지정 호스트와 포트 옵션을 지정할 수도 있다.

전반적으로 비동기 프로그래밍 지원과 동시 요청을 효율적으로 처리하는 기능 덕분에 Uvicorn은 고성능 Python 웹 어플리케이션을 실행하는 데 널리 사용된다.

## 전제 조건
시작하기 전에 시스템에 Python 3.7 이상이 설치되어 있는지 확인한다. 또한 프로젝트 종속성을 격리하기 위해 가상 환경을 설정하는 것을 권한다. 다음 명령을 실행하여 가상 환경을 만들 수 있다.

```bash
$ python3 -m venv myenv
```

가상 환경을 활성화한다.

macOS와 Linux의 경우,

```bash
$ source myenv/bin/activate
```

## 1 단계: 종속성 설치
필요한 종속성을 설치하는 것부터 시작하자. 터미널을 열고 다음 명령을 실행한다.

```bash
$ pip install starlette uvicorn
```

## 2 단계: 웹 머플리케이션 설치
이제 종속 요소가 설치되었으므로 `main.py`라는 새 Python 파일을 만들고, 즐겨 사용하는 코드 편집기로 이를 연다. 필요한 모듈을 가져오는 것부터 시작한다.

```python
from starlette.applications import Starlette
from starlette.responses import JSONResponse
from starlette.routing import Route
```

다음으로 JSON response로 응답하는 간단한 라우트 핸들러를 정의한다. `main.py` 파일에 다음 코드를 추가한다.

```python
async def hello_world(request):
    return JSONResponse({"message": "Hello, World!"})


routes = [
    Route("/", endpoint=hello_world)
]
app = Starlette(routes=routes)
```

위 코드에서는 `request` 객체를 매개변수로 받는 비동기 함수 `hello_world`를 정의한다. 이 함수는 간단한 "Hello, World!" 메시지가 포함된 JSON response을 반환한다.

그런 다음 `routes` 리스트를 정의하고, Starlette의 `Route` 클래스를 사용하여 각 경로를 정의한다. 
이 경우 루트 URL("/")을 `hello_world` 함수로 매핑하는 단일 경로만 있다.

마지막으로 Starlette 클래스의 인스턴스를 생성하고 경로 리스트를 생성자에게 전달한다.

## 3 단계: 웹 어플리케이션 실행
어플리케이션을 실행하기 위해 Uvicorn을 사용하겠다. 터미널을 열고 `main.py` 파일이 있는 디렉터리로 이동하여 다음 명령을 실행한다.

```bash
$ uvicorn main:app --reload
```

`main:app` 인수는 `main` 모듈에서 `app`이라는 이름의 오브젝트를 찾으라고 Uvicorn에 지시하며, 이 오브젝트는 Starlette 어플리케이션 인스턴스에 해당한다. 
`--reload` 플래그를 사용하면 소스 코드가 변경될 때마다 서버를 자동으로 다시 로드할 수 있다.

명령을 실행하면 다음과 유사한 출력이 표시된다.

```
INFO: Uvicorn running on http://127.0.0.1:8000 (Press CTRL+C to quit)
```

축하합니다! 이제 웹 어플리케이션이 실행 중이다.

## 4 단계: 어플리케이션 테스팅
어플리케이션을 테스트하려면 웹 브라우저를 열고 `http://localhost:8000` 을 방문한다. "Hello, World!" 메시지가 포함된 JSON response가 표시되어야 한다.

## 마치며
이 튜토리얼에서는 Starlette과 Uvicorn을 사용하여 간단한 웹 어플리케이션을 구축하는 방법을 살펴봤았다. 필요한 종속 요소를 설치하고, 기본 경로 처리기를 생성하고, 
Uvicorn을 사용하여 어플리케이션을 실행했다. Starlette과 Uvicorn은 Python에서 고성능 웹 애플리케이션을 개발하기 위한 강력한 조합을 제공한다.

여기에서 더 많은 경로를 추가하고 다양한 유형의 요청을 처리하여 어플리케이션을 확장할 수 있다. 또한 미들웨어, 인증, 데이터베이스 통합 등 
Starlette에서 제공하는 추가 기능도 살펴볼 수 있다. 각 기능에 대한 자세한 내용은 Starlette과 Uvicorn의 공식 문서를 참조하세요. Happy coding!


<a name="footnote_1">1</a>: 이 페이지는 [Building a Web Application with Starlette and Uvicorn: A Beginner’s Tutorial](https://code.likeagirl.io/building-a-web-application-with-starlette-and-uvicorn-a-beginners-tutorial-24eb43360594)을 편역한 것임.
