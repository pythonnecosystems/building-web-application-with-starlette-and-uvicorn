# Starlette 및 Uvicorn으로 웹 애플리케이션 구축하기: 초보자를 위한 튜토리얼 <sup>[1](#footnote_1)</sup>

이 튜토리얼에서는 가벼운 비동기 웹 프레임워크인 Starlette와 초고속 ASGI 서버인 Uvicorn을 사용하여 간단한 웹 어플리케이션을 구축하는 방법을 살펴보고자 한다. Starlette와 Uvicorn은 원활하게 함께 작동하므로 고성능 웹 어플리케이션을 쉽게 개발할 수 있다. [이제 시작하자](./building-web-application-with-starlette-and-uvicorn.md)!

<a name="footnote_1">1</a>: 이 페이지는 [Building a Web Application with Starlette and Uvicorn: A Beginner’s Tutorial](https://code.likeagirl.io/building-a-web-application-with-starlette-and-uvicorn-a-beginners-tutorial-24eb43360594)을 편역한 것임.
